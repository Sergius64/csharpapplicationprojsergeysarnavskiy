﻿using System;
using QAApplicationProj.Data.Entities;

namespace QAApplicationProj.Mappers
{
    public static class CarMapper
    {
        public static Car Map(Requests.CarRequest request) {
            if (request == null) {
                throw new ArgumentNullException(nameof(request));
            }

            return new Car() {
                Make = request.Make,
                Year = request.Year,
                Model = request.Model
            };
        }
    }
}
