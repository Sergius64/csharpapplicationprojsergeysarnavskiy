﻿namespace QAApplicationProj.Responses
{
    public class CreateCarResponse
    {
        public bool WasSuccessful { get; set; }
        public string Message { get; set; }
    }
}
