﻿using QAApplicationProj.Controllers;
using QAApplicationProj.Helpers;
using QAApplicationProj.Data.Repositories;
using Xunit;
using System;
using QAApplicationProj.Requests;
using QAApplicationProj.Responses;

namespace QAApplicationProj.Tests.Controllers
{
    public class CarControllerTests {
        private readonly CarController _controller;
        public CarControllerTests() {
            CarTestRepository ctr = new CarTestRepository();
            _controller = new CarController(ctr); 
        }

        // Checks that Validation catches Null CarRequest going in.
        [Fact]
        public void CreateCar_WasNotSuccessful_NullCarGiven()
        {
            CarRequest req = null;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The request did not pass validation." == response.Message);
        }

        // Checks that Validation catches it when Model is not declared.
        [Fact]
        public void CreateCar_WasNotSuccessful_NullModelGiven()
        {
            CarRequest req = new CarRequest();
            req.Make = "Ferrari";
            req.Year = 1989;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The request did not pass validation." == response.Message);
        }

        // Checks that Validation catches it when Make is not declared.
        [Fact]
        public void CreateCar_WasNotSuccessful_NullMakeGiven()
        {
            CarRequest req = new CarRequest();
            req.Model = "Testarossa";
            req.Year = 1989;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The request did not pass validation." == response.Message);
        }

        // Checks that Validation catches it when Year is not declared.
        [Fact]
        public void CreateCar_WasNotSuccessful_NullYearGiven()
        {
            CarRequest req = new CarRequest();
            req.Make = "Ferrari";
            req.Model = "Testarossa";
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The request did not pass validation." == response.Message);
        }

        // Checks that Validation catches it when Year is a negative value.
        [Fact]
        public void CreateCar_WasNotSuccessful_NegativeYearGiven()
        {
            CarRequest req = new CarRequest();
            req.Make = "Ferrari";
            req.Model = "Testarossa";
            req.Year = -1;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The request did not pass validation." == response.Message);
        }

        // Checks that Validation catches it when Make is left as an empty string.
        [Fact]
        public void CreateCar_WasNotSuccessful_EmptyMakeGiven()
        {
            CarRequest req = new CarRequest();
            req.Make = "";
            req.Model = "Testarossa";
            req.Year = 2000;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The request did not pass validation." == response.Message);
        }

        // Checks that Validation catches it when Make is left as an empty string.
        [Fact]
        public void CreateCar_WasNotSuccessful_EmptyModelGiven()
        {
            CarRequest req = new CarRequest();
            req.Make = "Ferrari";
            req.Model = "";
            req.Year = 2000;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The request did not pass validation." == response.Message);
        }

        // Checks that Validation catches it when Year is too far into the future.
        [Fact]
        public void CreateCar_WasNotSuccessful_YearTooFarIntoTheFuture()
        {
            CarRequest req = new CarRequest();
            req.Make = "Ferrari";
            req.Model = "Testarossa";
            req.Year = DateTime.Now.Year + 3;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The request did not pass validation." == response.Message);
        }

        // Checks that we do not allow the addition of a duplicate car and return a valid response.
        [Fact]
        public void CreateCar_WasNotSuccessful_ExistingCarGiven()
        {
            CarRequest req = new CarRequest();
            req.Make = "Honda";
            req.Model = "Civic";
            req.Year = 2010;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The requested Car already exists." == response.Message);
        }

        // Checks that we can successfully add a vehicle if only the year is different from an existing car.
        [Fact]
        public void CreateCar_WasSuccessful_NewYearSameModelAndMake()
        {
            CarRequest req = new CarRequest();
            req.Make = "Honda";
            req.Model = "Civic";
            req.Year = 2011;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.True(response.WasSuccessful);
        }

        // Checks that we can successfully add a vehicle if only the Model is different from an existing car.
        [Fact]
        public void CreateCar_WasSuccessful_NewModelSameMakeAndYear()
        {
            CarRequest req = new CarRequest();
            req.Make = "Honda";
            req.Model = "Accord";
            req.Year = 2010;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.True(response.WasSuccessful);
        }

        // Checks that we can successfully add a vehicle if only the Make is different from an existing car.
        [Fact]
        public void CreateCar_WasSuccessful_DifferentMakeButSameModelAndYear()
        {
            CarRequest req = new CarRequest();
            req.Make = "Toyota";
            req.Model = "Civic";
            req.Year = 2010;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.True(response.WasSuccessful);
        }

        // Check that we can add a car successfully and then get a failure when we try to add it again.
        [Fact]
        public void CreateCar_WasSuccessfulAndThenNot_CreatingSameCarTwice()
        {
            CarRequest req = new CarRequest();
            req.Make = "Mazda";
            req.Model = "Speed 3";
            req.Year = 2009;
            CreateCarResponse response = _controller.CreateCar(req);
            Assert.True(response.WasSuccessful);
            response = _controller.CreateCar(req);
            Assert.False(response.WasSuccessful);
            Assert.True("The requested Car already exists." == response.Message);
        }
    }
}
