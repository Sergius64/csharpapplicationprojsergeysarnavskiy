﻿using Xunit;
using System;
using MathNet.Numerics.Random;

namespace QAApplicationProj.Tests.Helpers
{
    public class FareHelperTests
    {
        private MersenneTwister randGen = new MersenneTwister();

        // Checks that we get an Argument out of range exception when first fare is negative for some reason.
        [Fact]
        public void AddFares_ReturnsArgumentOutOfRangeE_FirstDecimalNegative() {
            decimal negVal = GenerateRandomNegativeDecimal(), posVal = GenerateRandomPositiveDecimal();
            
            Assert.Throws<ArgumentOutOfRangeException>(() => QAApplicationProj.Helpers.FareHelper.AddFares(negVal, posVal));
        }

        // Checks that we get an Argument out of range exception when second fare is negative for some reason.
        [Fact]
        public void AddFares_ReturnsArgumentOutOfRangeE_SecondDecimalNegative()
        {
            decimal negVal = GenerateRandomNegativeDecimal(), posVal = GenerateRandomPositiveDecimal();

            Assert.Throws<ArgumentOutOfRangeException>(() => QAApplicationProj.Helpers.FareHelper.AddFares(posVal, negVal));
        }

        // Checks that we get an Argument out of range exception when both fares are negative for some reason.
        [Fact]
        public void AddFares_ReturnsArgumentOutOfRangeE_BothDecimalsNegative()
        {
            decimal negVal = GenerateRandomNegativeDecimal(), secondNegVal = GenerateRandomNegativeDecimal();

            Assert.Throws<ArgumentOutOfRangeException>(() => QAApplicationProj.Helpers.FareHelper.AddFares(negVal, secondNegVal));
        }

        // Checks that we get a Overflow Exception when the sum of the two decimals is over the Max Decimal value.
        [Fact]
        public void AddFares_ReturnsOverflowE_OneDecimalAtMax()
        {
            decimal posVal = GenerateRandomPositiveDecimal(), overflowVal = Decimal.MaxValue;

            Assert.Throws<OverflowException>(() => QAApplicationProj.Helpers.FareHelper.AddFares(posVal, overflowVal));
        }

        // Checks that if we add 25 different pairs of decimals that have sums below the max value, we do infact return the correct sums.
        [Fact]
        public void AddFares_ReturnsCorrectSum_ValidSums()
        {
            for ( int i = 0; i < 25; i++)
            {
                decimal[] validTestCase = GetSum();

                Assert.True(validTestCase[2] == QAApplicationProj.Helpers.FareHelper.AddFares(validTestCase[0], validTestCase[1]));
            }
        }

        /// <summary>
        /// Uses MathNet to generate a random negative decimal value.
        /// </summary>
        /// <returns>A random negative decimal value to be used in our tests.</returns>
        private decimal GenerateRandomNegativeDecimal()
        {
            decimal randomNegativeDecimal = randGen.NextFullRangeInt32() + randGen.NextDecimal();

            if (randomNegativeDecimal > 0)
                randomNegativeDecimal *= (-1);

            return randomNegativeDecimal;
        }

        /// <summary>
        /// Uses MathNet to generate a random positive decimal value.
        /// </summary>
        /// <returns>A random positive decimal value to be used in our tests.</returns>
        private decimal GenerateRandomPositiveDecimal()
        {
            decimal randomPositiveDecimal = randGen.NextFullRangeInt32() + randGen.NextDecimal();

            if (randomPositiveDecimal < 0)
                randomPositiveDecimal *= (-1);

            return randomPositiveDecimal;
        }

        /// <summary>
        /// Generates a valid TC where two random decimal values are NOT overflowing.
        /// </summary>
        /// <returns>An array with the valid Test Case, first element is first value, second element is second value and last element is the sum between the two.</returns>
        public decimal[] GetSum()
        {
            bool goodValsFound = false;
            decimal[] goodTCArray = new decimal[3];

            // We want to try to generate random decimals, but in some cases the sum might go over the top, in that case try the next set of random decimals.
            do
            {
                try
                {
                    goodTCArray[0] = GenerateRandomPositiveDecimal();
                    goodTCArray[1] = GenerateRandomPositiveDecimal();
                    goodTCArray[2] = goodTCArray[0] + goodTCArray[1];
                    goodValsFound = true;
                }
                catch (OverflowException) { }
            } while (!goodValsFound);

            return goodTCArray;
        }
    }
}
