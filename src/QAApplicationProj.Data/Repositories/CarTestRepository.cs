﻿using System;
using System.Collections.Generic;
using QAApplicationProj.Data.Entities;
using QAApplicationProj.Data.Repositories.Interfaces;

namespace QAApplicationProj.Data.Repositories
{
    public class CarTestRepository : ICarRepository
    {
        private static readonly ICollection<Car> Cars = new List<Car> {
            new Car() {
                Make = "Honda",
                Model = "Civic",
                Year = 2010
            },
            new Car() {
                Make = "Ferrari",
                Model = "Testarossa",
                Year = 1989
            },
            new Car() {
                Make = "Lamborghini",
                Model = "Murcielago",
                Year = 2006
            },
        };

        /// <summary>
        /// Accesses our small fake database to check if the sample car already exists.
        /// Note that the Make, Model, and Year are used for comparison in the Car's Equals() method.
        /// </summary>
        /// <param name="car"></param>
        /// <returns>true if the car exists</returns>
        public bool Exists(Car car)
        {
            if (car == null)
            {
                throw new ArgumentNullException(nameof(car));
            }

            return Cars.Contains(car);
        }

        /// <summary>
        /// Creates a car in our small fake database. Throws an exception if the Car already exists.
        /// </summary>
        /// <param name="car"></param>
        /// <returns>true if the car exists</returns>
        public void Create(Car car)
        {
            if (car == null)
            {
                throw new ArgumentNullException(nameof(car));
            }

            if (Exists(car))
            {
                throw new InvalidOperationException("Cannot create a Car that already exists.");
            }

            Cars.Add(car);
        }
    }
}
